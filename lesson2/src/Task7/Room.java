package Task7;

public class Room {
    private int basePrice = 40;
    private int liveDay = 1;
    private int totalPrice = liveDay * basePrice;
    private int saleUpToSevenDays = 20;
    private int saleSevenAndOverDays = 50;

    public Room() {

    }
    public Room(int liveDay) {
        this.liveDay = liveDay;
        this.totalPrice = liveDay * basePrice;
    }
    public int getTotalPrice() {
        if (liveDay >= 5 && liveDay < 7){
            return totalPrice - saleUpToSevenDays;
        } else if(liveDay >= 7){
            return  totalPrice - saleSevenAndOverDays;
        } else {
            return totalPrice;
        }
    }
}
