package Tasks1_2_3;

public class Task3 {
    public static void printAllMinimumNumbers(){
        Integer [] arr = Helpers.returnArrayWithThreeInteger();
        if (arr[0] == arr[1] && arr[0] == arr[2]){
            System.out.println("Min number: " + arr[0] + " and " + arr[1] + " and " + arr[2]);
        } else if (arr[0] == arr[1]){
            System.out.println("Min number: " + arr[0] + " and " + arr[1] );
        } else if (arr[0] == arr[2]){
            System.out.println("Min number: " + arr[0] + " and " + arr[2] );
        } else {
            System.out.println("Min number: " + arr[0]);
        }
    }
}
