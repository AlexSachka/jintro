package Task6;

public class Task6 {
    public static void ImmediateNumbers(int number1, int number2){

        int middleNumber = 10;
        int resultFirstNumber = middleNumber - number1;
        int resultSecondNumber = middleNumber - number2;

        if (resultFirstNumber == resultSecondNumber){
            System.out.println("Both numbers are equally close");
        } else if(resultFirstNumber < resultSecondNumber){
            System.out.println("Number: " + number1 + " is closely to " + middleNumber);
        } else if(resultFirstNumber > resultSecondNumber){
            System.out.println("Number: " + number2 + " is closely to " + middleNumber);
        }
    }
}
