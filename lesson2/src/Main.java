import Tasks1_2_3.Task1;
import Tasks1_2_3.Task2;
import Tasks1_2_3.Task3;
import Task4.Task4;
import Task5.Task5;
import Task6.Task6;
import Task7.*;
import java.io.IOException;

public class Main {
    public static void main(String[] args){
        /* Task1
        Найти минимальное из 3х чисел
        */
       // Task1.printMinNumber();

        /*Task2
        Найти среди 3х чисел максимальное и минимальное
        */
        //Task2.printMaxAndMinNumbers();

        /*Task3
        Найти минимальное из 3х чисел, причем,
        если минимальных несколько вывести все такие числа
        */
        //Task3.printAllMinimumNumbers();

        /*Task4
        Написать программу, которая проверит, является ли число четным или нечетным
        */
        //Task4.getEvenNumbers(5);

        /*Task5
        Даны 2 числа, определить делится ли первое число на второе без остатка
        */
        //Task5.getEvenDividedNumbers(78,33);

        /*Task 6
           Найти ближайшее к 10 из 2х заданных чисел.
           Например, среди чисел 8 и 11 ближайшее к десяти 11
        */
       // Task6.ImmediateNumbers(-9,-8);

        /*
         Посчитайте, сколько будет стоить аренда квартиры за заданное число дней,
          если известно, что за 1 день ее стоимость 40 грн. Причем,
          если съемщик прожил в ней больше 5 дней, он получит скидку 20 грн,
           а если больше 7 дней, то скидка составит 50 грн.
        */
        Room room = new Room(70);
        System.out.println(room.getTotalPrice());

    }
}
