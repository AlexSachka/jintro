package Task5;

public class Task5 {
    public static void getEvenDividedNumbers(int number1, int number2){
        int result = number1 % number2;
        if ( result == 0){
            System.out.println("Numbers are divided without remainder");
        } else {
            System.out.println("Remainder is: " + result);
        }
    }
}
